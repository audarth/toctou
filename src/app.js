"use strict";

// requirements
const express = require("express");

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get("/status", (req, res) => {
  res.status(200).end();
});
app.head("/status", (req, res) => {
  res.status(200).end();
});

// Send
app.get("/", (req, res) => {
  const initialBalance = 1000;

  const { balance, transfered } = transfer(initialBalance, req.query.amount);
  if (balance !== undefined || transfered !== undefined) {
    res
      .status(200)
      .end(
        "Successfully transfered: " + transfered + ". Your balance: " + balance
      );
  } else {
    res.status(400).end("Insufficient funds. Your balance: " + balance);
  }
});

// Transfer amount service
const transfer = (balance, amount) => {
  const parsedAmount = parseInt(amount, 10);

  if (parsedAmount <= 0) {
    return { balance: undefined, transfered: undefined };
  }

  const newBalance = balance - parsedAmount;
  if (newBalance < 0) {
    return { balance: undefined, transfered: undefined };
  } else {
    return { balance: newBalance, transfered: parsedAmount };
  }
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
  // HTTP listener
  app.listen(PORT, (err) => {
    if (err) {
      console.log(err);
      process.exit(1);
    }
    console.log("Server is listening on port: ".concat(PORT));
  });
}
// CTRL+c to come to action
process.on("SIGINT", function () {
  process.exit();
});

module.exports = { app, transfer };
